﻿/* Copyright (C) radynamics Reto Steimen, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential, v1.0.6
 * Written by Reto Steimen <rsteimen@radynamics.com>, 2020
 */
class QrZahlteil {
    constructor(apiKey, deviceName) {
        this._triggers = {};
		this._apiKey = apiKey == null ? "" : apiKey;
        this._deviceName = deviceName == null ? "WebClient" : deviceName;
        this._prop = null;
    }

    connect(prop) {
        console.log(prop);
		prop = (typeof prop !== "object") ? {} : prop;
		prop.sessionId = prop.sessionId || "";
		prop.transferPicture = prop.transferPicture || false;
        prop.acceptKodierzeile = prop.acceptKodierzeile || false;
        prop.identificationKey = prop.identificationKey || "";
		this._prop = prop;

        var instance = this
        this._socket = new WebSocket("wss://qrzahlteil.ch/api1/");
		//this._socket = new WebSocket("ws://localhost:8001/");
        this._socket.onopen = function(e) { instance.connectWs(); };
        this._socket.onmessage = function(event) { instance.handleMessage(event) };
        this._socket.onclose = function(event) {
            if (event.wasClean) {
                console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
            } else {
                // e.g. server process killed or network down event.code is usually 1006 in this case
                console.warn(`[close] Connection died, code=${event.code} reason=${event.reason}`);
            }
			instance.triggerHandler('disconnected', {});
        };
        this._socket.onerror = function(error) {
            console.error(`[error] ${error.message}`);
        };
    }

	// Behandelt vom Server empfangene Antworten und Meldungen
    handleMessage(msg) {
        console.log(`[message] Data received from server: ${msg.data}`);
        if(msg.data == null) {
            console.warn("Keine Daten erhalten");
            return;
        }
		var json = JSON.parse(msg.data);
		if(json.error != null) {
            console.error("Es ist ein Fehler aufgetreten, Code: " + json.error.code + ", Key: " + json.error.key);
            return;
        }
        var response = json.response;
        if(response == null || response.event == null) {
            console.warn("Keine Antwort oder kein Ereignis erhalten");
            return;
        }

		// Es wurde erfolgreich eine Verbindung zum Server hergestellt
        if (response.event == "connected") {
			// Speichern Sie die [sessionId] und übergeben Sie diese bei künftigen Verbindungen an "connect". Damit wird für den Benutzer eine automatische Verbindung zwischen Ihrem Programm und dem Smartphone ermöglicht.
            this._prop.sessionId = response.sessionId;
			// [pairingQrCodeImg] enthält ein Base64 kodiertes Bild als PNG, welches dem Benutzer zur Kopplung zwischen Smartphone und Ihrem Programm angezeigt werden muss.
			this.triggerHandler('connected', { sessionId: this._prop.sessionId, base64Png: response.pairingQrCodeImg });
        }

		// Der Benutzer hat das Smartphone erfolgreich mit Ihrem Programm verbunden
        if (response.event == "scanner_connected") {
			// [scannerName] enthält den Base64 kodierten Namen seines Smartphones.
            this.triggerHandler('scanner_connected', { scannerName: this.b64Decode(response.scannerName) });
        }

		// Der Benutzer hat eine QR-Rechnung erfolgreich gescannt
        if (response.event == "scanned") {
			// [data_base64] enthält den Base64 kodierten QR-Zahlteil der QR-Rechnung
			// [pictures_base64] enthält eine Liste von Base64 kodierten Bildern als JPEG, welche der Benutzer von der QR-Rechnung erstellt hat. Wenn bei "connect" Ihre Applikation mit dem Server [transferPicture] nicht definiert wurde oder false war, wird eine leerer Auflistung geliefert.
            this.triggerHandler('scanned', { raw: this.b64Decode(response.data_base64), picturesBase64: response.pictures_base64 });
        }

		// Der Benutzer hat die App auf dem Smartphone geschlossen
        if (response.event == "scanner_disconnected") {
            this.triggerHandler('scanner_disconnected', {});
        }
    }

    connectWs() {
        var json = {
            method: "connect",
            data: {
				// Gespeicherte sessionId oder leer ""
                sessionId: this._prop.sessionId,
				// True, falls der Benutzer die gesamte QR-Rechnung fotografieren soll und diese Fotos von Ihrer Applikation empfangen werden können
				transferPicture: this._prop.transferPicture,
                // True, falls die Kodierzeile von orangen/roten Einzahlungsscheinen empfangen werden kann
                acceptKodierzeile: this._prop.acceptKodierzeile,
                // Key als Freitext zur Identifikation einer Installation oder eines Anwenders, welcher zu jedem Scan im Klartext gespeichert wird.
                identificationKey: this._prop.identificationKey,
				// Base64 kodierter Name des Gerätes oder der Website
                deviceName: this.b64Encode(this._deviceName),
				// Ihr ApiKey (kann auf https://www.qrzahlteil.ch/ generiert werden)
				apiKey: this._apiKey
            }
        }
		// Das JSON Objekt als Text senden
        this._socket.send(JSON.stringify(json));
    }

    close() {
        this._socket.close(1000, "client_close");
    }

    on(event, callback) {
        if(!this._triggers[event]) {
            this._triggers[event] = [];
        }
        this._triggers[event].push(callback);
    }

    triggerHandler(event,params) {
        if(this._triggers[event] ) {
            for(var i in this._triggers[event]) {
                this._triggers[event][i](params);
            }
        }
    }

    b64Decode(str) {
        return decodeURIComponent(atob(str).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    }
    b64Encode(str) {
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode('0x' + p1);
        }));
    }
};