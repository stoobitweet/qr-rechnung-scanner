# QR Rechnung
This repository is used to get a Linux client or any other OS that can run python. It is based on the demo available in Javascript / html on https://www.qrzahlteil.ch/api

# Disclaimer
The software is provided wihtout any guarantee. I cannot be responsible if the app has a bad behaviour, if wrong data are sent to the app or any other situation that can cause wrong data to be displayed.

This software is for individual only. For commercial usage, please refer to https://www.qrzahlteil.ch/index.html#contact to contact the support.

PLEASE recheck the data before paying your invoces !
# Installation
Actually, it is a fully Python 3 application. It was tested with Python 3.6.9

## Requirements

You must install :
```bash
apt install python3 python3-tk
```

After that, you can clone the repository, install the requirements and run the app :
```bash
git clone https://gitlab.com/stoobitweet/qr-rechnung-scanner.git
cd qr-rechnung-scanner
pip3 install -r requirements.txt
```

You MUST generate an API key on https://www.qrzahlteil.ch/api and insert it in the variable API_KEY at the top of the `qrzahlteil.py` file like this :
```python
#########################################
# GLOBAL VARIABLES
REFRESH_WINDOW_TIME = 500 #ms
API_KEY = "THIS_IS_THE_API_KEY" # INSERT YOUR API KEY HERE !
#########################################
```

OR you can create a file in the same folder as the `qrzahlteil.py` file named `.api_key` and paste your key in this file.

## Usage
After that, you can run the code :
```
./qrzahlteil.py
```

And use the application on your smartphone to scan and get the data from your QR invoices.
