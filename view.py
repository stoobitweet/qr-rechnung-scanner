"""
This file is used to control the GUI of the app

"""


import tkinter as tk
import datetime
import json

class MainWindow():

    def __init__(self,title):
        self.window = tk.Tk() # Main window
        self.window.title = title

    def load_software(self,):
        """
        Method used to load the software_config.json file
        """
        with open('software_config.json', 'r') as myfile:
            data=myfile.read()

        return json.loads(data)

    def select_software(self,):
        """
        Method used to show a window to select the software from the list
        """
        # TODO: Open a window and display some list and select 
        self.soft_used = self.software_list['websites'][0]
        print("Software selected")


    def create_gui(self,):
        """
        Method used to create a GUI
        """
        menubar = tk.Menu(self.window,name="menubar")
        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Select software to use", command=self.select_software)
        filemenu.add_command(label="Save history to file...", command=self.save_history)
        filemenu.add_command(label="Close", command=self.quit)
        menubar.add_cascade(label="File", menu=filemenu)
        self.window.config(menu=menubar)

        self.lbl_status= tk.Label(self.window,textvariable=self.tk_status,name="lbl_status")
        self.lbl_status.pack()

        lbl_loading = tk.Label(self.window,text="Scan the QR code to connect your phone...",name="lbl_loading")
        lbl_loading.pack()

    def history(self):
        filewin = tk.Toplevel(self.window)
        button = tk.Button(filewin, text="Do nothing button")
        button.pack()
        filewin.update()

    def save_history(self):
        filename = str(datetime.datetime.now())+".txt"
        print("Saving history in {}".format(filename))
        f = open(filename,"w")

        for data in self.invoices_scanned:
            f.write("--------------------------------------------\n")
            for key,value in data.items():
                if value:
                    f.write("{} : {}\n".format(key,value))

        print("Save finished.")
        f.close()

    def quit(self,exit_code=0):
        self.window.quit()
        exit(exit_code)

