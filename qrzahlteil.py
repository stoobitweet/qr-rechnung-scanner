#!/usr/bin/env python3

"""
    :author: Sébastien Crettaz
    :description: Python class for QR ZahlTeil. No warranty given.
    :informations: This file is based on the Demo code given in https://qrzahlteil.ch/api.html
                   The original code is written by Reto Steimen.

"""
from view import MainWindow


from selenium import webdriver
from selenium.common.exceptions import ElementNotInteractableException
import websocket
from tkinter import *
import base64
import json
import threading
import time
import socket
import datetime

#########################################
# GLOBAL VARIABLES
REFRESH_WINDOW_TIME = 500 #ms
API_KEY = "" # INSERT YOUR API KEY HERE !
#########################################


class QrZahlteil():
    def __init__(self, apiKey, deviceName):
        """
        Constructor of class QrZahlteil
        """
        self._apiKey = apiKey if apiKey else ""
        self._deviceName = deviceName if deviceName else "Webclient"
        self._prop = {"sessionId":"","transferPicture":False,"acceptKodierzeile":True,"identificationKey":""} # Holding all data for the session
        self.invoices_scanned = []
        
        self.ws = websocket.WebSocketApp("wss://qrzahlteil.ch/api1/",
                                on_message = self.on_message,
                                on_error = self.on_error,
                                on_close = self.on_close,)
        self.window = Tk() # Main window
        self.window.title = "QR Rechnung - {}".format(self._deviceName) 
        self.window.minsize(1000, 300) 
        self.status = "not_connected"
        self.phone_status = "not_connected"
        self.tk_status = StringVar()
        self.tk_status.set("Status : "+self.phone_status)

        # Define the software used for invoices
        self.software_list = self.load_software()
        self.soft_used = "" 
        self.web_browser = None

        self.create_gui()


    def history(self):
        filewin = Toplevel(self.window)
        button = Button(filewin, text="Do nothing button")
        button.pack()
        filewin.update()

    def save_history(self):
        filename = str(datetime.datetime.now())+".txt"
        print("Saving history in {}".format(filename))
        f = open(filename,"w")

        for data in self.invoices_scanned:
            f.write("--------------------------------------------\n")
            for key,value in data.items():
                if value:
                    f.write("{} : {}\n".format(key,value))

        print("Save finished.")
        f.close()
    
    def quit(self,exit_code=0):
        self.window.quit()
        self.ws.close()
        exit(exit_code)


    def create_gui(self,):
        """
        Method used to create a GUI
        """
        menubar = Menu(self.window,name="menubar")
        filemenu = Menu(menubar, tearoff=0)
        filemenu.add_command(label="Select software to use", command=self.select_software)
        filemenu.add_command(label="Save history to file...", command=self.save_history)
        filemenu.add_command(label="Close", command=self.quit)
        menubar.add_cascade(label="File", menu=filemenu)
        self.window.config(menu=menubar)

        self.lbl_status= Label(self.window,textvariable=self.tk_status,name="lbl_status")
        self.lbl_status.pack()

        lbl_loading = Label(self.window,text="Scan the QR code to connect your phone...",name="lbl_loading")
        lbl_loading.pack()


    def connect(self,):
        """
        This method is used to get the QR Code image to connect the smartphone to the app.
        """

        data = {
            "method": "connect",
            "data": {
                "sessionId": self._prop["sessionId"],
                "transferPicture": self._prop["transferPicture"],
                "acceptKodierzeile": self._prop["acceptKodierzeile"],
                "identificationKey": self._prop["identificationKey"],
                "deviceName": base64.b64encode(self._deviceName.encode('utf-8')).decode('utf-8'),
                "apiKey": self._apiKey
            }
        }
        
        self.ws.send(json.dumps(data))

    def on_message(self, message):
        """
        Data coming from the WebSocket
        """
        message = json.loads(message)

        if "error" in message:
            print(message)

        status = message['response']['event']
        self.status = status

        print(status)

        if status == "connected":
            self._prop["sessionId"] = message["response"]["sessionId"]
            img = message["response"]["pairingQrCodeImg"]
            self._prop["qr_image"] = img

        elif status in ["scanner_connected","scanner_disconnected"]:
            self._prop["scannerName"] = base64.b64decode(message["response"]["scannerName"]).decode('utf-8')
            self.phone_status = status
        elif status == "scanned":
            text = base64.b64decode(message['response']['data_base64']).decode('utf-8')
            self._prop["scanned_data"] = text



    def on_error(self, error):
        print(error)

    def on_close(self,):
        print("### closed ###")

    def jsonify_scanned_invoice(self,data):
        """
        This method get the scanned data (invoice) and returns a JSON of the invoice to have a better understanding
        Based on this document : https://www.paymentstandards.ch/dam/downloads/ig-qr-bill-fr.pdf
        """
        retval = {
            "qrType":"",
            "version":"",
            "coding_type":"",
            "IBAN":"",
            "cre_address_type":"",
            "cre_name":"",
            "cre_street_1":"",
            "cre_house_num_1":"",
            "cre_zip":"",
            "cre_city":"",
            "cre_country":"",
            "cref_address_type":"",
            "cref_name":"",
            "cref_street_1":"",
            "cref_house_num_1":"",
            "cref_zip":"",
            "cref_city":"",
            "cref_country":"",
            "cref_amount":"",
            "cref_money":"",
            "debf_address_type":"",
            "debf_name":"",
            "debf_street_1":"",
            "debf_house_num_1":"",
            "debf_zip":"",
            "debf_city":"",
            "debf_country":"",
            "reference_type":"",
            "reference":"",
            "non_structured_com":"",
            "trailer":"",
            "invoice_info":"",
            "pa1_parameter":"",
            "pa2_parameter":"",
        }

        keys = list(retval.keys())
        lines = data.splitlines()

        # If there is one line, it means that a BVR was scanned
        # Otherwise, it's a QR code
        if len(lines) == 1:
            # Get amount
            split_lines = lines[0].split(">")
            cref_amount = split_lines[0][2:-1]
            #Add dot for cents
            cref_amount = cref_amount[:-2]+"."+cref_amount[-2:]
            cref_amount = float(cref_amount)
            retval["cref_amount"] = cref_amount

            # Get Reference number
            ref_number = split_lines[1].split("+")[0]
            retval["reference"] = ref_number

            # Get account to pay
            account = split_lines[1].split("+")[1].replace(">","")
            account = "{}-{}-{}".format(account[:3],account[3:-1],account[-1:])
            retval["account"] = account
        else:
            for idx, line in enumerate(lines):
                key = keys[idx]
                retval[key] = line

        # Add the scanned invoice in the list to have history
        if not retval in self.invoices_scanned:
            self.invoices_scanned.append(retval)
        
        return retval


    def update_window(self,):
        """
        Display the window depends on the status
        """
        if self.status == "connected":
            self.status = "idle"
            img = self._prop["qr_image"]
            
            image = PhotoImage(name="qr_code_connect",data=img)

            lbl = Label(self.window,image=image,name="qr_connect")
            lbl.image = image
            lbl.pack()

        # Scanner connected
        elif self.status == "scanner_connected":
            self.status = "idle"

            # Remove all child except label status and Menu
            for child in self.window.winfo_children():
                if child._name not in ["lbl_status","menubar","left_frame","right_frame"]:
                    child.destroy()

            # Update the label
            self.tk_status.set("Status : Connected with "+self._prop["scannerName"])
            self.lbl_status.config(fg='green')

        # Scanner disconnected
        elif self.status == "scanner_disconnected":
            # Update the status label
            self.tk_status.set("Status : {} disconnected.".format(self._prop["scannerName"]))
            self.lbl_status.config(fg='red')
                    
        # Scanned invoice
        elif self.status == "scanned":
            self.status = "idle"

            #print(self._prop['scanned_data'].encode('utf-8'))
            data_json = self.jsonify_scanned_invoice(self._prop['scanned_data'])
            #txt_data = Text(self.window,name="txt_data")
            has_been_created = False
            left_frame = None
            right_frame = None
            for child in self.window.winfo_children():
                if "left_frame" in child._name:
                    left_frame = child
                    has_been_created = True
                elif "right_frame" in child._name:
                    right_frame = child
                    has_been_created = True

            
            if not has_been_created:
                left_frame = Frame(self.window, bg='white',name="left_frame")
                left_frame.pack(side='left', fill=BOTH, padx=10, pady=5, expand=True)

                right_frame = Frame(self.window, bg='white',name="right_frame")
                right_frame.pack(side='right', fill=BOTH, padx=10, pady=5, expand=True)
            else:
                for child in left_frame.winfo_children():
                    child.destroy()
                for child in right_frame.winfo_children():
                    child.destroy()


            for key,value in data_json.items():
                if value:
                    #lbl_key = Label(left_frame,text=key).pack()
                    ent_key = Entry(left_frame, state='readonly', readonlybackground='white', fg='black', justify="right")
                    var = StringVar()
                    var.set(key)
                    ent_key.config(textvariable=var, relief='flat')
                    ent_key.pack(padx=10, pady=5, fill=X)


                    ent_value = Entry(right_frame, state='readonly', readonlybackground='white', fg='black')
                    var = StringVar()
                    var.set(value)
                    ent_value.config(textvariable=var, relief='flat')
                    ent_value.pack(padx=10, pady=5, fill=X)
                    #lbl_val = Label(right_frame,text=value)
                    #lbl_val.bind('<Button-1>',self.copy_to_clipboard)
                    #lbl_val.pack()


                    #str_insert = "{} : {}\n".format(key,value)
                    #txt_data.insert(INSERT,str_insert)
            #txt_data.pack()
            self._execute_payment()

        # Recall indefinitively this method to update everytime the display window
        self.window.after(REFRESH_WINDOW_TIME,self.update_window)

    def copy_to_clipboard(self,txt):
        print(txt)

    def load_software(self,):
        """
        Method used to load the software_config.json file
        """
        with open('software_config.json', 'r') as myfile:
            data=myfile.read()

        return json.loads(data)

    def select_software(self,):
        """
        Method used to show a window to select the software from the list
        """
        # TODO: Open a window and display some list and select 
        self.soft_used = self.software_list['websites'][0]
        print("Software selected")

    def _execute_payment(self,):
        """
        Execute payment 
        """
        data = self.invoices_scanned[-1]
        # TODO : Select the payment method and execute it
        # We must define a class for each payment method to avoid shit like this

        if self.soft_used:
            base_url = self.soft_used['base_url']
            if not self.web_browser:
                # WARNING : 'chromedriver' executable needs to be in PATH. Please see https://sites.google.com/a/chromium.org/chromedriver/home
                self.web_browser = webdriver.Chrome()
                self.web_browser.get(base_url+self.soft_used['login_url'])

                while self.web_browser.current_url != base_url+self.soft_used['logged_url']:
                    time.sleep(2)

            self.web_browser.get(base_url+self.soft_used['payment_url'])

            qr_field = None
            while not qr_field:
                try:
                    qr_field = self.web_browser.find_element(value="qr")
                except:
                    time.sleep(2)

            try:
                qr_field.click()
            except ElementNotInteractableException:
                time.sleep(1)
                qr_field.click()
                
            self.web_browser.find_element(value=self.soft_used['next_button']).click()

            element = None
            while not element:
                try:
                    element = self.web_browser.find_element(value=self.soft_used['iban_field'])
                except:
                    time.sleep(2)
            # Fill the form 
            self.web_browser.find_element(value=self.soft_used['iban_field']).send_keys(data["IBAN"])
            self.web_browser.find_element(value=self.soft_used['beneficiary_field']).send_keys(data["cre_name"]+"\n")
            self.web_browser.find_element(value=self.soft_used['beneficiary_field']).send_keys("{} {}\n".format(data["cre_street_1"],data["cre_house_num_1"]))
            self.web_browser.find_element(value=self.soft_used['beneficiary_field']).send_keys("{} {}".format(data["cre_zip"],data["cre_city"]))
            self.web_browser.find_element(value=self.soft_used['ref_number_field']).send_keys(data["reference"])
            self.web_browser.find_element(value=self.soft_used['reason_field']).send_keys(data["non_structured_com"])
            self.web_browser.find_element(value=self.soft_used['amount_field']).send_keys(data["cref_amount"])
            
        else:
            print("No software selected for payment...")




if __name__ == "__main__":
    """
    Main method
    """
    #websocket.enableTrace(True)
    if not API_KEY:
        try:
            f = open(".api_key","r")
            API_KEY = f.read()
            f.close()

            # If the user entered more than 32 chars (like a carriage return)
            # we set it to the 32 first chars
            if len(API_KEY) > 32:
                API_KEY = API_KEY[:32]
        except FileNotFoundError:
            print("No file '.api_key' found NOR API_KEY variable set")

        if not API_KEY:
            print("Please generate an API key from https://www.qrzahlteil.ch/api and insert it in the variable API_KEY.")
            exit(0)

    qr = QrZahlteil(API_KEY,socket.gethostname())
    #window = MainWindow("QR Zahlteil - {}".format(socket.gethostname()))
    
    # Create a thread to have the data of the websocket
    thread_websocket = threading.Thread(target=qr.ws.run_forever,daemon=True)
    thread_websocket.start()

    #thread_window = threading.Thread(target=window,daemon=True)
    #thread_window.start()

    conn_timeout = 5
    while not qr.ws.sock.connected and conn_timeout:
        print("wait...")
        time.sleep(1)
        conn_timeout -= 1

    if conn_timeout <= 0:
        print("Timeout connecting to the websocket...")
        qr.quit(exit_code=1)

    qr.connect()

    
    qr.window.after(REFRESH_WINDOW_TIME,qr.update_window)
    qr.window.mainloop()
    qr.quit()
